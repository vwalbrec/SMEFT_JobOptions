SMEFTparpam = {
    'cH'      :   +1.75351e+00,
    'cHDD'    :   -6.79370e+00,
    'cHbox'   :   +3.39686e+00,
    'cHW'     :   +2.10948e+00,
    'cHB'     :   +6.05469e-01,
    'cHWB'    :   +2.26028e+00,
    'cdHAbs'  :   +8.15541e-02,
    'cHl1'    :   +1.69843e+00,
    'cHq1'    :   -5.66143e-01,
    'cHl3'    :   +1.69843e+00,
    'cHq3'    :   +1.69843e+00,
    'cHd'     :   +1.13228e+00,
    'cHe'     :   +3.39686e+00,
    'cHu'     :   -2.26457e+00,
    'cuHAbs'  :   +3.37922e+00,
}
extrasPara = {
}

ModelCommand='-Czz1p2_massless'

include('MC15JobOptions/MadGraphControl_A14NNPDF23LO_VBFH4l_SMEFT_asScheme.py')
