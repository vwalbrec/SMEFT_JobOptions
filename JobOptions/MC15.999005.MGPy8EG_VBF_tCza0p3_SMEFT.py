
SMEFTparpam = {
    'cH'      :   0.000000e+00,
    'cHDD'    :   0.000000e+00,
    'cHbox'   :   0.000000e+00,
    'cHG'     :   0.000000e+00,
    'cHW'     :   0.000000e+00,
    'cHWtil'  :   +2.35221e-01,
    'cHB'     :   0.000000e+00,
    'cHBtil'  :   -2.35221e-01,
    'cHWB'    :   0.000000e+00,
    'cHWBtil' :   -3.13035e-01,
    'cHu'     :   0.000000e+00
}


extrasPara = {
}

ModelCommand='-tCza0p3_massless'

include('MC15JobOptions/MadGraphControl_A14NNPDF23LO_VBFH4l_SMEFT.py')
