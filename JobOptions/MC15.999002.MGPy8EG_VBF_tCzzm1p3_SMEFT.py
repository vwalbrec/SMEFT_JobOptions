
SMEFTparpam = {
    'cH'      :   0.000000e+00,
    'cHDD'    :   0.000000e+00,
    'cHbox'   :   0.000000e+00,
    'cHG'     :   0.000000e+00,
    'cHW'     :   0.000000e+00,
    'cHWtil'  :   -2.28527e+00,
    'cHB'     :   0.000000e+00,
    'cHBtil'  :   -6.55925e-01,
    'cHWB'    :   0.000000e+00,
    'cHWBtil' :   -2.44864e+00,
    'cHu'     :   0.000000e+00
}
#

extrasPara = {
}

ModelCommand='-tCzzm1p3_massless'

include('MC15JobOptions/MadGraphControl_A14NNPDF23LO_VBFH4l_SMEFT.py')
