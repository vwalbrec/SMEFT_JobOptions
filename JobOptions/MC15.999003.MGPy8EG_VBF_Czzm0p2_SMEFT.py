
SMEFTparpam = {
    'cH'      :   -2.92251e-01,
    'cHDD'    :   +1.13228e+00,
    'cHbox'   :   -5.66143e-01,
    'cHW'     :   -3.51580e-01,
    'cHB'     :   -1.00912e-01,
    'cHWB'    :   -3.76714e-01,
    'cdHAbs'  :   -1.35923e-02,
    'cHl1'    :   -2.83071e-01,
    'cHq1'    :   +9.43571e-02,
    'cHl3'    :   -2.83071e-01,
    'cHq3'    :   -2.83071e-01,
    'cHd'     :   -1.88715e-01,
    'cHe'     :   -5.66143e-01,
    'cHu'     :   +3.77428e-01,
    'cuHAbs'  :   -5.63203e-01,
}

extrasPara = {
}

ModelCommand='-Czzm0p2_massless'

include('MC15JobOptions/MadGraphControl_A14NNPDF23LO_VBFH4l_SMEFT.py')
