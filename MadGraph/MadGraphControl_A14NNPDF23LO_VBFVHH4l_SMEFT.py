from MadGraphControl.MadGraphUtils import *
import math

safefactor=1.1

#---------------------------------------------------------------------------------------------------
# generating process cards
#---------------------------------------------------------------------------------------------------

fcard = open('proc_card_mg5.dat','w')


#---------------------------------------------------------------------------------------------------
# generating VBF
#---------------------------------------------------------------------------------------------------
# import model SMEFTsim_A_U35_alphaScheme_UFO
fcard.write("""
import model SMEFTsim_A_U35_MwScheme_UFO_v2_1"""+ModelCommand+"""

define v = a z w+ w-
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~

generate p p > j j h QCD=0 NP=1, h > l+ l- l+ l- NP=1

output -f
""")
fcard.close()




# require beam energy to be set as argument
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process()


#---------------------------------------------------------------------------------------------------
# creating run_card.dat for VBF
#---------------------------------------------------------------------------------------------------
ptj             = '10.'
eta_max_pdg     = '{}'
ptj1min         = '0.0'
ptj1max         = '-1.0'
mmjjmin         = '5.'
mmjjmax         = '-1.0'
deltaeta        = '0.0'
pt_min_pdg      = '{}'
pt_max_pdg      = '{}'

if 'ptj' in extrasPara:
    ptj=extrasPara['ptj']
if 'eta_max_pdg' in extrasPara:
    eta_max_pdg=extrasPara['eta_max_pdg']
if 'ptj1min' in extrasPara:
    ptj1min=extrasPara['ptj1min']
if 'ptj1max' in extrasPara:
    ptj1max=extrasPara['ptj1max']
if 'mmjjmin' in extrasPara:
    mmjjmin=extrasPara['mmjjmin']
if 'mmjjmax' in extrasPara:
    mmjjmax=extrasPara['mmjjmax']
if 'deltaeta' in extrasPara:
    deltaeta=extrasPara['deltaeta']
if 'pt_min_pdg' in extrasPara:
    pt_min_pdg=extrasPara['pt_min_pdg']
if 'pt_max_pdg' in extrasPara:
    pt_max_pdg=extrasPara['pt_max_pdg']


extras = {'lhe_version'    :'3.0',
          'pdlabel'        : "'nn23lo1'",
          'lhaid'          : 230000,
          'parton_shower'  :'PYTHIA8',
          # 'ickkw'          : '1',
          # 'auto_ptj_mjj'   : 'True',
          'event_norm'     : 'sum',
          'cut_decays'     : 'T', #False     ! Cut decay products
          'drll'           : '0.0', #0.4 ! min distance between leptons
          'bwcutoff'       : '50', #15.0  ! (M+/-bwcutoff*Gamma)
          'pta'            : '0.', #10 ! minimum pt for the photons
		  'ptl'            : '0.', #10 ! minimum pt for the photons
          'etal'           : '-1.0', # 2.5 max rap for the charged leptons
          'drjj'           : '0.0', #0.4   ! min distance between jets
          'draa'           : '0.0',
          'etaj'           : '-1.0',
          'draj'           : '0.0',
          'drjl'           : '0.0',
          'dral'           : '0.0',
          'etaa'           : '-1.0',
          'ptj'            : ptj, #! minimum pt for the jets
          'eta_max_pdg'    : eta_max_pdg, #'{25: 2.5}'
          'ptj1min'        : ptj1min, #! minimum pt for the leading jet in pt
          'ptj1max'        : ptj1max, #! maximum pt for the leading jet in pt
          'mmjj'           : mmjjmin, #! min invariant mass of a jet pair
          'mmjjmax'        : mmjjmax, #max invariant mass of a jet pair
          'deltaeta'       : deltaeta, #! minimum rapidity for two jets in the WBF case
          'pt_min_pdg'     : pt_min_pdg #pt cut for other particles (use pdg code). Applied on particle and anti-particle
          }

# from https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#Problems_with_run_card_dat_in_ne
# build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', xqcut=20.0,
#                nevts=int(runArgs.maxEvents*safefactor),rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=int(runArgs.maxEvents*safefactor),rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
print_cards()

#---------------------------------------------------------------------------------------------------
# Setting EFT parameters in SMEFT
#---------------------------------------------------------------------------------------------------
#SMEFTParams
cHG =0.0 #ggF
cHW =0.0 #Hgaga, HZZ,WH, ZH, VBF
cHB =0.0 #Hgaga, WH, ZH, VBF
cHWB=0.0 #Hgaga, WH, ZH, VBF
cHu =0.0 #ggF, ttH
# #set values of parameters
# # cHG
if 'cHG' in SMEFTparpam:
    cHG=SMEFTparpam['cHG']
# # cHW
if 'cHW' in SMEFTparpam:
    cHW=SMEFTparpam['cHW']
# # cHB
if 'cHB' in SMEFTparpam:
    cHB=SMEFTparpam['cHB']
# # cHWB
if 'cHWB' in SMEFTparpam:
    cHWB=SMEFTparpam['cHWB']
# # cHu
if 'cHu' in SMEFTparpam:
    cHu=SMEFTparpam['cHu']
    # MadGraph_param_card_SMEFT-SMlimit_massless_MWschemeEFT.dat
paramcard = subprocess.Popen(['get_files','-data','MadGraph_param_card_SMEFT-SMlimit_massless_MWschemeEFT.dat'])
# paramcard = subprocess.Popen(['get_files','-data','MadGraph_param_card_SMEFT-SMlimit_massless.dat'])
paramcard.wait()
# if not os.access('MadGraph_param_card_SMEFT-SMlimit_massless.dat',os.R_OK):
if not os.access('MadGraph_param_card_SMEFT-SMlimit_massless_MWschemeEFT.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    # oldcard = open('MadGraph_param_card_SMEFT-SMlimit_massless.dat','r')
    oldcard = open('MadGraph_param_card_SMEFT-SMlimit_massless_MWschemeEFT.dat','r')
    newcard = open('param_card.dat','w')

    for line in oldcard:
        if '26 0.000000e+00' in line:
            newcard.write('   26 %e # cHG \n'%(cHG))
        elif '28 0.000000e+00' in line:
            newcard.write('   28 %e # cHW \n'%(cHW))
        elif '30 0.000000e+00' in line:
            newcard.write('   30 %e # cHB \n'%(cHB))
        elif '32 0.000000e+00' in line:
            newcard.write('   32 %e # cHWB \n'%(cHWB))
        elif '50 0.000000e+00' in line:
            newcard.write('   50 %e # cHu \n'%(cHu))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

runName='run_01'

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name=runName,proc_dir=process_dir)
# Multi-core capability
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

# option: disable TestHepMC
# if hasattr(testSeq, "TestHepMC"):
#     testSeq.remove(TestHepMC())

#### Showering with Pythia 8
evgenConfig.description = "VBFVH SMEFT"
evgenConfig.keywords = ["Higgs","VBF"]
evgenConfig.process = "generate p p > h j j QCD=0"
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
evgenConfig.contact = ["Verena Maria Walbrecht <vwalbrec@cern.ch>"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")


# option to enable CKKWL jet merging - needs to be used in combination with ickkw=0
# PYTHIA8_nJetMax=2
# PYTHIA8_Dparameter=0.4
# PYTHIA8_Process='pp>n1n1hs'
# PYTHIA8_TMS=30
# PYTHIA8_nQuarksMerge=4
# include("MC15JobOptions/Pythia8_CKKWL_kTMerge.py")

# particle data = name antiname spin=2s+1 3xcharge colour mass width (left out, so set to 0: mMin mMax tau0)
# genSeq.Pythia8.Commands += ["SLHA:allowUserOverride = on",
#                             "1000022:all = chi chi~ 2 0 0 %d 0.0 0.0 0.0 0.0" %(mdm),
#                             "1000022:isVisible = false"]

# ## MET filter
# include("MC15JobOptions/MissingEtFilter.py")
# filtSeq.MissingEtFilter.METCut = 50*GeV
# filtSeq.Expression = "(MissingEtFilter)"
