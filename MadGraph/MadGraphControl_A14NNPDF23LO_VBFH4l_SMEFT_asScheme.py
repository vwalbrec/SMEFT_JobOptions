from MadGraphControl.MadGraphUtils import *
import math

safefactor=1.1

#---------------------------------------------------------------------------------------------------
# generating process cards
#---------------------------------------------------------------------------------------------------

fcard = open('proc_card_mg5.dat','w')



#---------------------------------------------------------------------------------------------------
# generating VBF H4l
#---------------------------------------------------------------------------------------------------

fcard.write("""
import model SMEFTsim_A_U35_alphaScheme_UFO_v2_1"""+ModelCommand+"""

define v = a z w+ w-
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~

generate p p > j j h $$ v QCD=0 NP=1, h > l+ l- l+ l- NP=1

output -f
""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process()


#--------------------------------------------------------cmt br cmt make-------------------------------------------
# creating run_card.dat for VBF
#---------------------------------------------------------------------------------------------------
ptj             = '15.'
eta_max_pdg     = '{}'
ptj1min         = '0.0'
ptj1max         = '-1.0'
mmjjmin         = '0.'
mmjjmax         = '-1.0'
deltaeta        = '0.0'
pt_min_pdg      = '{}'
pt_max_pdg      = '{}'

if 'ptj' in extrasPara:
    ptj=extrasPara['ptj']
if 'eta_max_pdg' in extrasPara:
    eta_max_pdg=extrasPara['eta_max_pdg']
if 'ptj1min' in extrasPara:
    ptj1min=extrasPara['ptj1min']
if 'ptj1max' in extrasPara:
    ptj1max=extrasPara['ptj1max']
if 'mmjjmin' in extrasPara:
    mmjjmin=extrasPara['mmjjmin']
if 'mmjjmax' in extrasPara:
    mmjjmax=extrasPara['mmjjmax']
if 'deltaeta' in extrasPara:
    deltaeta=extrasPara['deltaeta']
if 'pt_min_pdg' in extrasPara:
    pt_min_pdg=extrasPara['pt_min_pdg']
if 'pt_max_pdg' in extrasPara:
    pt_max_pdg=extrasPara['pt_max_pdg']

extras = {'lhe_version'    :'3.0',
          'pdlabel'        : "'nn23lo1'",
          'lhaid'          : 230000,
          # 'parton_shower'  :'PYTHIA8',
          # 'ickkw'          : '1',
          # 'auto_ptj_mjj'   : 'True',
          'event_norm'     : 'sum',
          'cut_decays'     : 'T', #False     ! Cut decay products
          'drll'           : '0.05', #0.4 ! min distance between leptons
          'bwcutoff'       : '50', #15.0  ! (M+/-bwcutoff*Gamma)
          'pta'            : '0.', #10 ! minimum pt for the photons
		  'ptl'            : '4.', #10 ! minimum pt for the photons
          'etal'           : '-1.0', # 2.5 max rap for the charged leptons
          'drjj'           : '0.0', #0.4   ! min distance between jets
          'draa'           : '0.0',
          'etaj'           : '-1.0',
          'draj'           : '0.0',
          'drjl'           : '0.0',
          'dral'           : '0.0',
          'etaa'           : '-1.0',
          # 'use_syst'       : 'False',
          'ptj'            : ptj, #! minimum pt for the jets
          'eta_max_pdg'    : eta_max_pdg, #'{25: 2.5}'
          'ptj1min'        : ptj1min, #! minimum pt for the leading jet in pt
          'ptj1max'        : ptj1max, #! maximum pt for the leading jet in pt
          'mmjj'           : mmjjmin, #! min invariant mass of a jet pair
          'mmjjmax'        : mmjjmax, #max invariant mass of a jet pair
          'deltaeta'       : deltaeta, #! minimum rapidity for two jets in the WBF case
          'pt_min_pdg'     : pt_min_pdg #pt cut for other particles (use pdg code). Applied on particle and anti-particle
          }
#
# from https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#Problems_with_run_card_dat_in_ne
# build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', xqcut=0.0,
#                nevts=int(runArgs.maxEvents*safefactor),rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=int(runArgs.maxEvents*safefactor),rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
print_cards()

#---------------------------------------------------------------------------------------------------
# Setting EFT parameters in SMEFT
#---------------------------------------------------------------------------------------------------
#SMEFTParams
cH=0.0
cHbox=0.0
cHDD=0.0
cHG =0.0 #ggF
cHW =0.0 #Hgaga, HZZ,WH, ZH, VBF
cHWtil=0.0
cHB =0.0 #Hgaga, WH, ZH, VBF
cHBtil=0.0
cHWB=0.0 #Hgaga, WH, ZH, VBF
cHWBtil=0.0
cHu =0.0 #ggF, ttH
cdHAbs=0.0
cHl1=0.0
cHq1=0.0
cHl3=0.0
cHq3=0.0
cHd=0.0
cHe=0.0
# cHu=0.0
cuHAbs=0.0
# LambdaSMEFT = 2.460000e+02
LambdaSMEFT = 1.000000e+03



# #set values of parameters
if 'cH' in SMEFTparpam:
    cH=SMEFTparpam['cH']

if 'cHbox' in SMEFTparpam:
    cHbox=SMEFTparpam['cHbox']

if 'cHDD' in SMEFTparpam:
    cHDD=SMEFTparpam['cHDD']

if 'cHG' in SMEFTparpam:
    cHG=SMEFTparpam['cHG']

if 'cHW' in SMEFTparpam:
    cHW=SMEFTparpam['cHW']

if 'cHWtil' in SMEFTparpam:
    cHWtil=SMEFTparpam['cHWtil']

if 'cHB' in SMEFTparpam:
    cHB=SMEFTparpam['cHB']

if 'cHBtil' in SMEFTparpam:
    cHBtil=SMEFTparpam['cHBtil']

if 'cHWB' in SMEFTparpam:
    cHWB=SMEFTparpam['cHWB']

if 'cHWBtil' in SMEFTparpam:
    cHWBtil=SMEFTparpam['cHWBtil']

if 'cHu' in SMEFTparpam:
    cHu=SMEFTparpam['cHu']

if 'cdHAbs' in SMEFTparpam:
    cdHAbs=SMEFTparpam['cdHAbs']

if 'cHl1' in SMEFTparpam:
    cHl1=SMEFTparpam['cHl1']

if 'cHq1' in SMEFTparpam:
    cHq1=SMEFTparpam['cHq1']

if 'cHl3' in SMEFTparpam:
    cHl3=SMEFTparpam['cHl3']

if 'cHq3' in SMEFTparpam:
    cHq3=SMEFTparpam['cHq3']

if 'cHd' in SMEFTparpam:
    cHd=SMEFTparpam['cHd']

if 'cHe' in SMEFTparpam:
    cHe=SMEFTparpam['cHe']

# if 'cHu' in SMEFTparpam:
#     cHu=SMEFTparpam['cHu']

if 'cuHAbs' in SMEFTparpam:
    cuHAbs=SMEFTparpam['cuHAbs']



paramcard = subprocess.Popen(['get_files','-data','MadGraph_param_card_SMEFT-SMlimit_massless.dat'])
# paramcard = subprocess.Popen(['get_files','-data','restrict_tCzz1p3_massless.dat'])
paramcard.wait()
if not os.access('MadGraph_param_card_SMEFT-SMlimit_massless.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('MadGraph_param_card_SMEFT-SMlimit_massless.dat','r')
    newcard = open('param_card.dat','w')

    for line in oldcard:
        if '18 1.000000e+03' in line:
            newcard.write('   18 %e # LambdaSMEFT \n'%(LambdaSMEFT))
        elif '23 0.000000e+00' in line:
            newcard.write('   23 %e # cH \n'%(cH))
        elif '24 0.000000e+00' in line:
            newcard.write('   24 %e # cHbox \n'%(cHbox))
        elif '25 0.000000e+00' in line:
            newcard.write('   25 %e # cHDD \n'%(cHDD))
        elif '26 0.000000e+00' in line:
            newcard.write('   26 %e # cHG \n'%(cHG))
        elif '28 0.000000e+00' in line:
            newcard.write('   28 %e # cHW \n'%(cHW))
        elif '29 0.000000e+00' in line:
            newcard.write('   29 %e # cHWtil \n'%(cHWtil))
        elif '30 0.000000e+00' in line:
            newcard.write('   30 %e # cHB \n'%(cHB))
        elif '31 0.000000e+00' in line:
            newcard.write('   31 %e # cHBtil \n'%(cHBtil))
        elif '32 0.000000e+00' in line:
            newcard.write('   32 %e # cHWB \n'%(cHWB))
        elif '33 0.000000e+00' in line:
            newcard.write('   33 %e # cHWBtil \n'%(cHWBtil))
        # elif '50 0.000000e+00' in line:
        #     newcard.write('   50 %e # cHu \n'%(cHu))
        elif '36 0.000000e+00' in line:
            newcard.write('   36 %e # cdHAbs \n'%(cdHAbs))
        elif '45 0.000000e+00' in line:
            newcard.write('   45 %e # cHl1 \n'%(cHl1))
        elif '48 0.000000e+00' in line:
            newcard.write('   48 %e # cHq1 \n'%(cHq1))
        elif '46 0.000000e+00' in line:
            newcard.write('   46 %e # cHl3 \n'%(cHl3))
        elif '49 0.000000e+00' in line:
            newcard.write('   49 %e # cHq3 \n'%(cHq3))
        elif '51 0.000000e+00' in line:
            newcard.write('   51 %e # cHd \n'%(cHd))
        elif '47 0.000000e+00' in line:
            newcard.write('   47 %e # cHe \n'%(cHe))
        elif '50 0.000000e+00' in line:
            newcard.write('   50 %e # cHu \n'%(cHu))
        elif '35 0.000000e+00' in line:
            newcard.write('   35 %e # cuHAbs \n'%(cuHAbs))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

runName='run_01'



generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name=runName,proc_dir=process_dir)
# Multi-core capability
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

# option: disable TestHepMC
# if hasattr(testSeq, "TestHepMC"):
#     testSeq.remove(TestHepMC())

#### Showering with Pythia 8
evgenConfig.description = "VBF H4l SMEFT"
evgenConfig.keywords = ["Higgs","VBF"]
evgenConfig.process = "generate p p > j j h $$ v QCD=0 NP=1, h > l+ l- l+ l- NP=1"
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
evgenConfig.contact = ["Verena Maria Walbrecht <vwalbrec@cern.ch>"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
